.. _interface_python:

Python interface
****************

This section includes documentation of some of the internal methods and classes
can be useful for further analysis.


.. index::
   single: Function reference; Fourier transformations
   single: Function reference; Filon's method

Fourier transformations
-----------------------

.. automodule:: dsf.filon
   :members:
   :undoc-members:
   :inherited-members:



.. index::
   single: Function reference; Trajectory handling

Trajectory handling
===================

.. automodule:: dsf.trajectory
   :members:
   :undoc-members:
   :inherited-members:
