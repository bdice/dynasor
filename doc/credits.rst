.. index:: Credits

.. |br| raw:: html

  <br/>


Credits
*******

:program:`dynasor` has been originally developed by Mattias Slabanja in the Materials and Surface Theory division at the Department of Physics at Chalmers University of Technology. Copyright and credit belong to him.
The documentation including the examples was written by Erik Fransson.
The development of this package has been supported by the Knut och Alice Wallenbergs Foundation, the Swedish Research Council, the Swedish Foundation for Strategic Research, and the Swedish National Infrastructure for Computing.

When using :program:`dynasor` in your research please cite the following paper:

* *Dynasor – A tool for extracting dynamical structure factors and current correlation functions from molecular dynamics simulations* |br|
  Erik Fransson, Mattias Slabanja, Paul Erhart, and Göran Wahnström |br|
  Advanced Theory and Simulations **4**, 2000240 (2021); DOI:`10.1002/adts.202000240 <https://doi.org/10.1002/adts.202000240>`_
