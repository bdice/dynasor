#!/bin/sh

DYNASOR=dynasor

K_MAX=20     		 # Consider kspace from gamma (0) to K_MAX inverse nanometer
K_BINS=155      	 # Collect result using K_BINS "bins" between 0 and $K_MAX
TIME_WINDOW=2000  	 # Consider time correlations up to TIME_WINDOW trajectory frames
MAX_FRAMES=25000   	 # Read at most MAX_FRAMES frames from trajectory file (then stop)

dt=$((5*1)) # This needs to be correspond to lammps timestep * dumpFreq * $STEP in fs in order to get the units correct.

TRAJECTORY="lammpsrun/dump/dumpT1400.NVT.atom.velocity"
OUTPUT="outputs/dynasor_outT1400_dynamical"

${DYNASOR} -f "$TRAJECTORY" \
    --k-bins=$K_BINS \
	--k-max=$K_MAX \
	--max-frames=$MAX_FRAMES \
	--nt=$TIME_WINDOW \
	--dt=$dt \
    --om=$OUTPUT.m




